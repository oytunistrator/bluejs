import Mustache from 'micromustache';

export default class Navigation {
  constructor(bluejs) {
    this.__navigation = null;
    this.__root = null;
    this.__error = null;
    this.__bjs = bluejs;
  };

  path() {
    return window.location.hash.split('#')[1];
  };

  hashToArray() {
    return this.path().split('/');
  };

  hashChange(func) {
    return window.addEventListener('hashchange', function (e) {
      if (typeof func === 'function') {
        func();
      }
    }, false);
  };

  render(template, params) {
    var temp = this.__bjs.select(template).html();
    // Mustache.parse(temp);
    var rendered = Mustache.render(temp, params);
    this.__bjs.select(template).html(rendered);
  };

  run() {
    var _nav = this.__navigation;
    var _root = this.__root;
    var template, params, func, urlPath;

    if ('onhashchange' in window) {
      window.onhashchange = function () {
        urlPath = window.location.hash.split('#')[1];
        if (typeof _nav === 'object' && urlPath !== undefined) {
          for (var i in _nav) {
            if (urlPath === _nav[i].path) {
              if (typeof _nav[i].fn === 'function') {
                func = _nav[i].fn;
                func();
              }
              if (typeof _nav[i].render === 'object') {
                template = _nav[i].render.template;
                params = _nav[i].render.template;

                this.render(template, params);
              }
            }
          }
        } else {
          if (typeof _root.fn === 'function') {
            func = _root.fn;
            func();
          }
        }
      };
    }
  };

  config(selector, nav, options) {
    this.__navigation = nav;
    this.__selector = this.__bjs.select(selector);
    this.options = this.__bjs.extend(options);

    /* TODO: Make conf generator */

    this.__navigation = nav;
    var selected = this.__bjs.select(selector);
    this.options = options;

    if (typeof this.options.root === 'object') {
      this.__root = this.options.root;
    }
    var parent = document.createElement('ul');
    if (typeof this.options.parent === 'string') {
      parent = document.createElement(this.options.parent);
    }
    if (typeof this.options.class === 'string') {
      parent.className = this.options.class;
    }
    if (typeof this.options.id === 'string') {
      parent.id = this.options.id;
    }
    if (typeof nav !== 'object') {
    } else {
      for (var element in nav) {
        var child = document.createElement('li');
        if (typeof this.options.child === 'string') {
          child = document.createElement(this.options.child);
        }
        if (typeof nav[element].class === 'string') {
          child.className = nav[element].class;
        }
        if (typeof nav[element].id === 'string') {
          child.id = nav[element].id;
        }
        if (typeof nav[element].link === 'object') {
          var a = document.createElement('a');
          a.href = '#' + nav[element].path;
          if (typeof nav[element].link.class === 'string') {
            a.className = nav[element].link.class;
          } else {
            nav[element].link.class = nav[element].link.text.replace(/ /g, '').toLowerCase();
            a.className = nav[element].link.class;
          }
          if (typeof nav[element].link.id === 'string') {
            a.id = nav[element].link.id;
          }
          if (typeof nav[element].link.text === 'string') {
            a.innerHTML = nav[element].link.text;
          }
          child.innerHTML = a.outerHTML;
        } else {
          if (typeof nav[element].content === 'string') {
            child.href = '#' + nav[element].path;
            child.innerHTML = nav[element].content;
          }
        }
        parent.innerHTML = parent.innerHTML + child.outerHTML;
      }

      selected.html(parent.outerHTML);

      this.run();
    }

  };
}
