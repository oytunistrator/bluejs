/**
 * Created by oytun on 17/10/2016.
 */
export default class Socket {
  constructor(url, options) {
    this.inspect = window.console;
    this.error = this.inspect.error;
    this.msg = this.inspect.msg;
    this._url = url;
    this._options = this.extend(options);

    if ('WebSocket' in window) {
      if (typeof this._url === 'string') {
        this._socket = new WebSocket(this._url);
        this._socket.onopen = this.open(event);
        this._socket.onmessage = this.message(event);
        this._socket.onclose = this.close(event);
      } else {
        this.inspect.error('Your url doesn\'t string.');
      }
    } else {
      this.inspect.error('Your browser doesn\'t support this function.');
    }

    return this._socket;
  }

  open(event) {
    if (typeof this._options.open === 'function') {
      _open = this._options.open;
      _open(event);
    }
  }

  message(event) {
    if (typeof this._options.message === 'function') {
      _message = this._options.message;
      _message(event);
    }
  }

  close(event) {
    if (typeof this._options.close === 'function') {
      _close = this._options.close;
      _close(event);
    }
  }

  send(message) {
    return this._socket.send(message);
  }

  extend(obj, name) {
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        if (name !== undefined) {
          this[name][i] = obj[i];
        } else {
          this[i] = obj[i];
        }
      }
    }

    return this;
  }
}
