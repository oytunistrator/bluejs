import $ from 'jquery';
import Xhr from './Xhr.js';
import Navigation from './Navigation';
import Socket from './Socket.js';

export default class BlueJS {
  constructor() {
    this._name = 'BlueJS';
    this.navigation = new Navigation(this);
    this.inspect = window.console;
    this.fn = this;
  }

  get name() {
    return this._name;
  }

  scriptLoad(url, onload, location){
    var scriptTag = document.createElement('script');
    if(undefined !== url) {
      scriptTag.src = url;
    }
    if(undefined !== onload) {
      scriptTag.onload = onload;
      scriptTag.onreadystatechange = onload;
    }
    if(undefined !== location) {
      location.appendChild(scriptTag);
    }else{
      document.head.appendChild(scriptTag);
    }
    return scriptTag;
  }

  loadjs(url, onload, location){
    if(url.constructor === Array){
      for(var i = 0; i < url.length; i++) {
        this.scriptLoad(url[i], onload, location);
      }
    }else{
      this.scriptLoad(url, onload, location);
    }
  }

  xhr(options) {
    return new Xhr(options);
  }

  j(options){
    return $(options);
  }

  extend(obj, name) {
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        if (name !== undefined) {
          this[name][i] = obj[i];
        } else {
          this[i] = obj[i];
        }
      }
    }
    return this;
  }

  date(params) {
    if (params) {
      return new Date(params);
    }
    return new Date();
  }

  map(callback) {
    var results = [], i = 0;
    for (; i < this.element.length; i++) {
      results.push(callback.call(this.element, this.element[i], i));
    }
    return results;
  }

  mapOne(callback) {
    var m = this.map(callback);
    return m.length > 1 ? m : m[0];
  }

  forEach(callback) {
    this.map(callback);
    return this;
  }

  html(html) {
    if (typeof html !== 'undefined') {
      this.forEach(function (el) {
        el.innerHTML = html;
      });
      return this;
    }
    return this.mapOne(function (el) {
      return el.innerHTML;
    });
  }

  addClass(className, options) {
    var $top = this;
    if (typeof className !== 'undefined') {
      this.forEach(function (el) {
        if (!$top.hasClass(el, className)) {
          el.className += ' ' + className;
        }
        if (typeof options === 'object') {
          $top.createClass('.' + className, options);
        }
      });
      return this;
    }
    return this.mapOne(function (el) {
      return el.className;
    });
  }

  removeClass(className) {
    var $top = this;
    if (typeof className !== 'undefined') {
      this.forEach(function (el) {
        if ($top.hasClass(el, className)) {
          el.className = el.className.replace(new RegExp('(?:^|\\s)' + className + '(?!\\S)'), '');
        }
      });
      return this;
    }
    return this.mapOne(function (el) {
      return el.className;
    });
  }

  createClass(name, rules) {
    var style = document.createElement('style');
    style.type = 'text/css';
    var content = name + '{';
    for (var i in rules) {
      if (rules.hasOwnProperty(i)) {
        content += i + ':' + rules[i] + ';';
      }
    }
    content += '}';
    style.innerText = content;
    document.getElementsByTagName('head')[0].appendChild(style);
    return this;
  }

  hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
  }

  select(selector) {
    if (selector !== undefined) {
      if (typeof selector === 'string') {
        this.element = document.querySelectorAll(selector);
      } else if (selector.length) {
        this.element = selector;
      } else {
        this.element = [selector];
      }
    }
    return this;
  }

  socket(url, options) {
    return new Socket(url, options);
  }

  info(message) {
    if (typeof window.console === 'object') {
      window.console.info(message);
    }
  }

  log(message) {
    if (typeof window.console === 'object') {
      window.console.log(message);
    }
  }

  error(error) {
    if (typeof window.console === 'object') {
      window.console.error(error);
    }
  }

  getObjectClass(obj) {
    if (typeof obj !== 'object' || obj === null) {
      return false;
    }
    return /(\w+)\(/.exec(obj.constructor.toString())[1];
  }

  scriptLoader(urls) {
    for (var i in urls) {
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = urls[i];
      document.getElementsByTagName('head')[0].appendChild(script);
    }
  }

  render(template, params) {
    return this.navigation.render(template, params);
  }
}
