var m = new Metruga();
var nav = m.navigation;
var j = m.j;

/* Render Sources */
m.render(".main",{
  footer: "This is dynamic footer!",
  hello: "Hello World!",
  title: "Dynamic Metruga Template!",
  details: "View details...",
  learnmore: "Learn more...",
  desc: "This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique."
});

/* Navbar Config */
nav.config('#navbar',[
  {
    path: '/index',
    link: {
      text: 'Main Page'
    },
    fn: function(){
      m.inspect.info("Main Page Loaded!");
      m.select("h1.hello").html("Main Page");
    }
  },
  {
    path: '/testPage',
    link: {
      text: 'Test Page'
    },
    fn: function(){
      m.inspect.info("Test Page Loaded!");
      m.select("h1.hello").html("Test Page");
    }
  },
  {
    path: '/about',
    link: {
      text: 'About'
    },
    fn: function(){
      m.inspect.info("About Loaded!");
      m.select("h1.hello").html("About");
    }
  }
],{
  class: 'nav navbar-nav',
  parent: 'ul',
  child: 'li',
  root: {
    fn: function(){
      m.inspect.info("Hello World Loaded!");
      m.select("h1.hello").html("Hello World");
    }
  }
});